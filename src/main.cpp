#include <Windows.h>
#include <vector>
#include <string>
#include <iostream>
#include <conio.h>

using namespace std;

struct File
{
	string name;
	string extension;
	string path;
	bool isDir;
};

int system(string cmd)
{
	return system(cmd.c_str());
}

vector<File> listFiles(string path)
{
	vector<File> tmpList;
	WIN32_FIND_DATAA data;

	if (!path.empty() && path[path.size() - 1] != '\\')
		path.append("\\");

	HANDLE finder = FindFirstFileA((path + "*").c_str(), &data);

	while (FindNextFileA(finder, &data))
	{
		if (!strcmp(data.cFileName, "..") || data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)
			continue;

		File file;
		file.isDir = data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY;
		file.path = path;
		file.name = string(data.cFileName);

		if (file.isDir)
		{
			file.path += file.name;
		}
		else
		{
			size_t ext = file.name.find_last_of('.');

			if (ext != string::npos)
				file.extension = file.name.substr(ext, string::npos);
			file.name = file.name.substr(0, ext);
		}

		tmpList.push_back(file);
	}

	FindClose(finder);

	return tmpList;
}

void writeIMG(string destIMG, vector<File> & files)
{
	FILE * image = fopen(destIMG.c_str(), "wb");
	FILE * src;
	int imgSize = 0;
	char blank = 0;
	
	for (auto & f : files)
	{
		src = fopen((f.path + f.name).c_str(), "rb");
		if (src == nullptr)
			continue;

		int size;

		fseek(src, 0, SEEK_END);
		size = ftell(src);
		fseek(src, 0, SEEK_SET);

		char * buffer = new char[size];

		fread(buffer, size, 1, src);
		fwrite(buffer, size, 1, image);

		delete[] buffer;

		fclose(src);

		imgSize += size;
	}

	for (int i = imgSize; i < 1474560; i++)
		fwrite(&blank, 1, 1, image);

	fclose(image);
}

void build(string srcDir, string binDir)
{
	string cmd;
	for (auto & f : listFiles(srcDir))
	{
		if (f.isDir)
		{
			system("md " + binDir + "\\" + f.name);
			build(srcDir + "\\" + f.name, binDir + "\\" + f.name);
			continue;
		}

		if (f.extension == ".c")
		{
			cmd = "i686-elf-gcc -ffreestanding -nostdlib -masm=intel -c -o \"" + binDir + "\\" + f.name + ".o\" \"" + srcDir + "\\" + f.name + f.extension + "\"";
			cout << "Building " + srcDir + "\\" + f.name + f.extension << endl;
			system(cmd);
			cout << endl;
		}
		else if (f.extension == ".asm")
		{
			cmd = "fasm \"" + srcDir + "\\" + f.name + f.extension + "\" \"" + binDir + "\\" + f.name + ".o\"";
			cout << "Building " + srcDir + "\\" + f.name + f.extension << endl;
			system(cmd);
			cout << endl;
		}
	}
}

void clean(string dir, vector<string> extensions)
{
	for (auto & f : listFiles(dir))
	{
		if (f.isDir)
		{
			clean(f.path, extensions);
			continue;
		}

		bool isMatching = false;
		for (auto & ext : extensions)
			if (f.extension == ext)
			{
				isMatching = true;
				break;
			}

		if (isMatching)
		{
			cout << "Deleting " << f.path + f.name + f.extension << endl;
			system("del \"" + f.path + f.name + f.extension);
		}
	}
}

void main(int argc, char *argv[])
{
	cout << "TnEBuid (C) 2014 by yuraSniper" << endl;

	if (argc < 2)
	{
		cout << "Usage : TnEBuild build <srcDir> <binDir>" << endl;
		cout << "        TnEBuild clean [<dir>]" << endl;
		cout << "        TnEBuild link <output file> <linkerScript> <list of files>" << endl;
		cout << "        TnEBuild writeIMG <img file> <list of files>" << endl;
		return;
	}

	cout << endl;

	if (!strcmp(argv[1], "build"))
	{
		if (argc < 4)
		{
			cout << "Error! Not enough parameters!" << endl;
			return;
		}

		cout << "Building from source dir : " << argv[2] << endl;

		build(argv[2], argv[3]);
	}
	else if (!strcmp(argv[1], "clean"))
	{
		string dir = "";
		if (argc > 2)
			dir = argv[2];

		cout << "Cleaning " << argv[2] << endl;
		clean(dir, {".o", ".bin"});
	}
	else if (!strcmp(argv[1], "link"))
	{
		if (argc < 5)
		{
			cout << "Error! Not enough parameters!" << endl;
			return;
		}

		string outPath = argv[2];
		outPath = outPath.substr(0, outPath.find_last_of('\\'));

		string files = "";
		for (int i = 4; i < argc; i++)
			files.append(string(argv[i]) + " ");

		cout << "Linking " << files << "into " << argv[2] << endl;

		system("i686-elf-ld -T " + string(argv[3]) + " -o " + argv[2] + " " + files);
	}
	else if (!strcmp(argv[1], "writeIMG"))
	{
		if (argc < 4)
		{
			cout << "Error! Not enough parameters!" << endl;
			return;
		}

		string imgPath = argv[2];
		size_t pos = imgPath.find_last_of('\\');

		if (pos != string::npos)
			imgPath = imgPath.substr(0, pos);
		else
			imgPath = "";

		vector<File> files;
		string fileList = "";

		for (int i = 3; i < argc; i++)
		{
			string name = argv[i];
			string path = name.substr(0, name.find_last_of('\\'));
			name = name.substr(path.size(), string::npos);

			files.push_back({name, "", path, false});
			fileList.append(string(argv[i]) + " ");
		}

		cout << "Making " << argv[2] << " from " << fileList << endl;

		writeIMG(argv[2], files);
	}
	else
		cout << "Error! Undefined command : \"" << argv[1] << "\"" << endl;

	cout << endl << endl;
}